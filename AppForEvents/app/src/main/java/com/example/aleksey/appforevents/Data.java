package com.example.aleksey.appforevents;


import com.example.aleksey.appforevents.POJO.Authdevice;
import com.example.aleksey.appforevents.POJO.Event;
import com.example.aleksey.appforevents.POJO.Feed;
import com.example.aleksey.appforevents.POJO.Worker;

import java.util.ArrayList;


public class Data{

    static ArrayList<String> list_names_of_events = new ArrayList();
    static ArrayList<Event> list_of_events = new ArrayList();
    static ArrayList<String> buf_list = new ArrayList();
    static ArrayList<String> list_names_of_workers = new ArrayList();
    static ArrayList<Worker> list_of_workers = new ArrayList();
    static ArrayList<Feed> buf_list_of_events = new ArrayList();
    static ArrayList<String> list_names_of_authdevices = new ArrayList();
    static ArrayList<Authdevice> list_of_authdevices = new ArrayList();

}