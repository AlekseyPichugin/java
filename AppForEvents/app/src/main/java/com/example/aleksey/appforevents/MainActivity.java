package com.example.aleksey.appforevents;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.aleksey.appforevents.POJO.Feed;
import com.example.aleksey.appforevents.POJO.Radiation;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ListOfEvents.OnEventsInteractionListener,
        ListOfWorkers.OnWorkersInteractionListener,
        ListOfAuthDevices.OnAuthDevicesInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        UpdateListOfEvents();
        //ListOfEvents.get_List_Of_Events();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void onFilterClick (MenuItem item){
        showDialog(1);
    }

    @Override
    protected Dialog onCreateDialog(final int id) {
        final boolean[] mCheckedItems = {false, false};
        final String[] mItems = {"Null", "Not null"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Отобразить:").setCancelable(false)
                .setMultiChoiceItems(mItems,mCheckedItems,new DialogInterface.OnMultiChoiceClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        mCheckedItems[which] = isChecked;
                        Log.d("Checked","is ok");
                    }
                }).setPositiveButton("Ok",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setNegativeButton("Cancel",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        Log.d("Dialog","is ok");
        return builder.create();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.list_of_events) {
            //ListOfEvents.get_List_Of_Events();
            UpdateListOfEvents();
        } else if (id == R.id.list_of_workers) {
            //ListOfWorkers.get_List_Of_Workers();
            UpdateListOfWorkers();
        } else if (id == R.id.list_of_authdevices) {
            //ListOfAuthDevices.get_List_Of_AuthDevices();
            UpdateListOfAuthDevices();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onEventsInteraction(int position) {
        EventsDetails fragment = new EventsDetails();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_main, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    @Override
    public void onWorkersInteraction(int position) {
        WorkersDetail fragment = new WorkersDetail();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_main, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onAddWorkerInteraction() {
        AddWorker fragment = new AddWorker();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_main, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onAuthDevicesInteraction(int position) {
        AuthDevicesDetails fragment = new AuthDevicesDetails();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_main, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void UpdateListOfEvents(){
        ListOfEvents list_of_events = new ListOfEvents();
        FragmentTransaction ftrans = getSupportFragmentManager().beginTransaction();
        ftrans.replace(R.id.content_main,list_of_events);
        ftrans.addToBackStack(null);
        ftrans.commit();
        ListOfEvents.get_List_Of_Events();
    }

    public void UpdateListOfWorkers(){
        ListOfWorkers list_of_workers = new ListOfWorkers();
        FragmentTransaction ftrans = getSupportFragmentManager().beginTransaction();
        ftrans.replace(R.id.content_main,list_of_workers);
        ftrans.addToBackStack(null);
        ftrans.commit();
        ListOfWorkers.get_List_Of_Workers();
    }

    public void UpdateListOfAuthDevices(){
        ListOfAuthDevices list_of_authdevices = new ListOfAuthDevices();
        FragmentTransaction ftrans = getSupportFragmentManager().beginTransaction();
        ftrans.replace(R.id.content_main,list_of_authdevices);
        ftrans.addToBackStack(null);
        ftrans.commit();
        ListOfAuthDevices.get_List_Of_AuthDevices();
    }

}
