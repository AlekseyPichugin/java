package com.example.aleksey.appforevents;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Aleksey on 06.02.2017.
 */

public class EventsDetails extends android.support.v4.app.Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.events_details, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        setText(args.getInt("position"));
    }

    // обновление текстового поля
    public void setText(int position) {
        TextView authdevice_id = (TextView) getActivity().findViewById(R.id.eventdetails_device_id);
        TextView event_time = (TextView) getActivity().findViewById(R.id.eventdetails_event_time);
        TextView event_type = (TextView) getActivity().findViewById(R.id.eventdetails_event_type);

        authdevice_id.setText("ID устройства: " + Data.list_of_events.get(position).getAuthdeviceId());
        event_time.setText("Время: " + Data.list_of_events.get(position).getEventTime());
        event_type.setText("Тип события: " + Data.list_of_events.get(position).getEventType());
    }
}
