package com.example.aleksey.appforevents;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.aleksey.appforevents.POJO.Worker;

import java.util.HashMap;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class AddWorker extends Fragment {

    Integer REQUEST_CODE = 1;
    private Integer position = -1;
    static String url = "http://192.168.59.43/";
    TextView authdevice;
    EditText f_name, s_name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_workers, container, false);
        return view;
}
    @Override
    public void onStart() {
        super.onStart();

        authdevice = (TextView)getActivity().findViewById(R.id.add_worker_device);
        authdevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddWorker.this.getActivity(), DetailsActivity.class);
                startActivityForResult(intent,REQUEST_CODE);
            }
        });

        Button clear = (Button)getActivity().findViewById(R.id.add_worker_reset);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear_fields();
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE){
            Integer answer = data.getIntExtra("Id",-1);
            if(answer != -1){
                /*Data.list_of_workers.get(position).setWorkerAuthdeviceId(Data.list_of_authdevices
                        .get(answer).getAuthdeviceId());*/

                Log.d("Request",String.valueOf(Data.list_of_authdevices
                        .get(answer).getAuthdeviceId()));
            }
        }
    }

    public void clear_fields(){
        f_name = (EditText)getActivity().findViewById(R.id.add_worker_fname);
        s_name = (EditText)getActivity().findViewById(R.id.add_worker_sname);
        authdevice = (TextView) getActivity().findViewById(R.id.add_worker_device);

        f_name.setText("");
        s_name.setText("");
        authdevice.setText("Добавить устройство");
    }
}
