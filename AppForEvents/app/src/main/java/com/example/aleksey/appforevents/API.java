package com.example.aleksey.appforevents;

import com.example.aleksey.appforevents.POJO.AuthDevices;
import com.example.aleksey.appforevents.POJO.Authdevice;
import com.example.aleksey.appforevents.POJO.Events;
import com.example.aleksey.appforevents.POJO.Radiation;
import com.example.aleksey.appforevents.POJO.Worker;
import com.example.aleksey.appforevents.POJO.Workers;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Map;

import retrofit.Call;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PartMap;


public interface API {
    @FormUrlEncoded
    @POST("site.php/get_all_events")
    Call<Events> get_all_events(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("site.php/get_all_workers")
    Call<Workers> get_all_workers(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("site.php/add_worker")
    Call<Workers> add_worker(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("site.php/get_all_authdevices")
    Call<AuthDevices> get_all_authdevices(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("site.php/change_authdevice")
    Call<Worker> change_authdevice(@FieldMap Map<String,Integer> map);
}
