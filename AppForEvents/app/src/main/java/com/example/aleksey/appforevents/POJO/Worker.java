
package com.example.aleksey.appforevents.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Worker {

    @SerializedName("worker_id")
    @Expose
    private Integer workerId;
    @SerializedName("worker_fname")
    @Expose
    private String workerFname;
    @SerializedName("worker_sname")
    @Expose
    private String workerSname;
    @SerializedName("worker_authdevice_id")
    @Expose
    private Integer workerAuthdeviceId;

    public Integer getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }

    public String getWorkerFname() {
        return workerFname;
    }

    public void setWorkerFname(String workerFname) {
        this.workerFname = workerFname;
    }

    public String getWorkerSname() {
        return workerSname;
    }

    public void setWorkerSname(String workerSname) {
        this.workerSname = workerSname;
    }

    public Integer getWorkerAuthdeviceId() {
        return workerAuthdeviceId;
    }

    public void setWorkerAuthdeviceId(Integer workerAuthdeviceId) {
        this.workerAuthdeviceId = workerAuthdeviceId;
    }

}
