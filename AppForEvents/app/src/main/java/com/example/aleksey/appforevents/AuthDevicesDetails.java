package com.example.aleksey.appforevents;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Aleksey on 06.02.2017.
 */

public class AuthDevicesDetails extends android.support.v4.app.Fragment{

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.authdevices_details, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        setText(args.getInt("is_enabled"));
    }

    // обновление текстового поля
    public void setText(int position) {
        TextView hardwawe_id = (TextView) getActivity().findViewById(R.id.authdevicedetails_authdevice_id);
        TextView access_level = (TextView) getActivity().findViewById(R.id.authdevicedetails_acces_level);

        hardwawe_id.setText("ID устройства: " + String.valueOf(Data.list_of_authdevices
                .get(position).getHardwareId()));
        access_level.setText("Уровень доступа: " + String.valueOf(Data.list_of_authdevices
                .get(position).getAccessLevel()));

    }
}
