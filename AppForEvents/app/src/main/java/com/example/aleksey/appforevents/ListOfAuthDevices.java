package com.example.aleksey.appforevents;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.aleksey.appforevents.POJO.AuthDevices;
import com.example.aleksey.appforevents.POJO.Authdevice;
import com.example.aleksey.appforevents.POJO.Worker;
import com.example.aleksey.appforevents.POJO.Workers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Aleksey on 06.02.2017.
 */

public class ListOfAuthDevices extends ListFragment implements SwipeRefreshLayout.OnRefreshListener{
    private OnAuthDevicesInteractionListener AuthDevicesListener;
    private SwipeRefreshLayout AuthDevicesSwipeRefreshLayout;
    public static ArrayAdapter<String> AuthDevicesListAdapter;

    public interface OnAuthDevicesInteractionListener {

        public void onAuthDevicesInteraction(int position);
        public void UpdateListOfAuthDevices();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AuthDevicesListAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, Data.list_names_of_authdevices);
        setListAdapter(AuthDevicesListAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.list_of_authdevices, null);
        AuthDevicesSwipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipeAuthDevicesList);
        AuthDevicesSwipeRefreshLayout.setOnRefreshListener(this);
        return layout;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            AuthDevicesListener = (OnAuthDevicesInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " должен реализовывать интерфейс OnFragmentInteractionListener");
        }
    }

    public void onListItemClick (ListView l, View v, int pos, long id) {
        super.onListItemClick(l, v, pos, id); {
            updateDetail(pos);
        }
    }

    public void updateDetail(int position) {
        AuthDevicesListener.onAuthDevicesInteraction(position);
    }

    static String url = "http://192.168.59.43/";
    public static void get_List_Of_AuthDevices(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API service = retrofit.create(API.class);
        Map<String,String> mapJsn = new HashMap<>();
        mapJsn.put("table","workers");
        Call<AuthDevices> call = service.get_all_authdevices(mapJsn);

        call.enqueue(new Callback<AuthDevices>() {
            @Override
            public void onResponse(Response<AuthDevices> response, Retrofit retrofit) {
                Data.list_names_of_authdevices.clear();
                Data.list_of_authdevices.clear();
                for (Authdevice i : response.body().getAuthdevices()) {
                    Data.list_names_of_authdevices.add("Authdevice_Id: " + i.getAuthdeviceId()
                            +"\nHardware_Id: "+i.getHardwareId());
                    Data.list_of_authdevices.add(i);
                }
                AuthDevicesListAdapter.notifyDataSetChanged();
                Log.d("Get Workers", "done");

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onRefresh() {
        AuthDevicesSwipeRefreshLayout.setRefreshing(true);
        AuthDevicesSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                AuthDevicesSwipeRefreshLayout.setRefreshing(false);
                //add_Worker("Николай","Трофимов","8");
                get_List_Of_AuthDevices();

            }
        }, 1000);

    }
}
