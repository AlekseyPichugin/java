
package com.example.aleksey.appforevents.POJO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthDevices {

    @SerializedName("table")
    @Expose
    private String table;
    @SerializedName("authdevices")
    @Expose
    private List<Authdevice> authdevices = null;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<Authdevice> getAuthdevices() {
        return authdevices;
    }

    public void setAuthdevices(List<Authdevice> authdevices) {
        this.authdevices = authdevices;
    }

}
