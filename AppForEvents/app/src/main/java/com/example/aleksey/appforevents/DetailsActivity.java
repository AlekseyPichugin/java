package com.example.aleksey.appforevents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by alexey on 11.02.2017.
 */

public class DetailsActivity extends AppCompatActivity implements
        ListOfAuthDevices.OnAuthDevicesInteractionListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.details_activity);

        UpdateListOfAuthDevices();
       // ListOfAuthDevices.get_List_Of_AuthDevices();

    }


    @Override
    public void onAuthDevicesInteraction(int position) {
        /*AuthDevicesDetails fragment = new AuthDevicesDetails();
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_details_activity, fragment);
        transaction.addToBackStack(null);
        transaction.commit();*/
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra("Id",position);
        setResult(RESULT_OK, intent);
        finish();
    }

    public void UpdateListOfAuthDevices(){
        ListOfAuthDevices list_of_authdevices = new ListOfAuthDevices();
        FragmentTransaction ftrans = getSupportFragmentManager().beginTransaction();
        ftrans.replace(R.id.content_details_activity,list_of_authdevices);
        ftrans.addToBackStack(null);
        ftrans.commit();
        ListOfAuthDevices.get_List_Of_AuthDevices();
    }


}
