package com.example.aleksey.appforevents;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.aleksey.appforevents.POJO.Event;
import com.example.aleksey.appforevents.POJO.Events;
import com.example.aleksey.appforevents.POJO.Feed;
import com.example.aleksey.appforevents.POJO.Radiation;

import java.util.HashMap;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ListOfEvents extends ListFragment implements SwipeRefreshLayout.OnRefreshListener {

    private OnEventsInteractionListener mListener;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public static ArrayAdapter<String> adapter;
    public interface OnEventsInteractionListener {

        public void onEventsInteraction(int position);
        public void UpdateListOfEvents();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, Data.list_names_of_events);
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.list_of_events, null);
        mSwipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipeMovieHits);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return layout;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnEventsInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " должен реализовывать интерфейс OnFragmentInteractionListener");
        }
    }
    final static String url = "http://192.168.59.43/";
    public static void get_List_Of_Events(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API service = retrofit.create(API.class);
        Map<String,String> mapJsn = new HashMap<>();
        mapJsn.put("table","events");
        Call<Events> call = service.get_all_events(mapJsn);

            call.enqueue(new Callback<Events>() {
                @Override
                public void onResponse(Response<Events> response, Retrofit retrofit) {
                    Data.list_names_of_events.clear();
                    Data.list_of_events.clear();
                    for (Event i : response.body().getEvents()) {
                        Data.list_names_of_events.add(i.getEventType()+"\n"+i.getEventTime());
                        Data.list_of_events.add(i);
                    }
                    adapter.notifyDataSetChanged();
                    Log.d("Get Events", "done");
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });

    }

    public void onListItemClick (ListView l, View v, int pos, long id) {
        super.onListItemClick(l, v, pos, id); {
            updateDetail(pos);
        }
    }

    public void updateDetail(int position) {
        mListener.onEventsInteraction(position);
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(false);
                //mListener.get_Events_Report();
                get_List_Of_Events();
            }
        }, 1000);

    }

}
