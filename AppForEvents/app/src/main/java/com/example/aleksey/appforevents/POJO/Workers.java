
package com.example.aleksey.appforevents.POJO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Workers {

    @SerializedName("table")
    @Expose
    private String table;
    @SerializedName("workers")
    @Expose
    private List<Worker> workers = null;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }

}
