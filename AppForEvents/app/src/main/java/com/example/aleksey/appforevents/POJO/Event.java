
package com.example.aleksey.appforevents.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Event {

    @SerializedName("event_id")
    @Expose
    private String eventId;
    @SerializedName("authdevice_id")
    @Expose
    private String authdeviceId;
    @SerializedName("event_time")
    @Expose
    private String eventTime;
    @SerializedName("event_type")
    @Expose
    private String eventType;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getAuthdeviceId() {
        return authdeviceId;
    }

    public void setAuthdeviceId(String authdeviceId) {
        this.authdeviceId = authdeviceId;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

}
