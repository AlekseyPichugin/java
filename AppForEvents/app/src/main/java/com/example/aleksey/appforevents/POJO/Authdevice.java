
package com.example.aleksey.appforevents.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Authdevice {

    @SerializedName("authdevice_id")
    @Expose
    private Integer authdeviceId;
    @SerializedName("access_level")
    @Expose
    private Integer accessLevel;
    @SerializedName("hardware_id")
    @Expose
    private Integer hardwareId;

    public Integer getAuthdeviceId() {
        return authdeviceId;
    }

    public void setAuthdeviceId(Integer authdeviceId) {
        this.authdeviceId = authdeviceId;
    }

    public Integer getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(Integer accessLevel) {
        this.accessLevel = accessLevel;
    }

    public Integer getHardwareId() {
        return hardwareId;
    }

    public void setHardwareId(Integer hardwareId) {
        this.hardwareId = hardwareId;
    }

}
