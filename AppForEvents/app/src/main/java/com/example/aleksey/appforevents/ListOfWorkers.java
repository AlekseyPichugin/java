package com.example.aleksey.appforevents;

import android.app.Activity;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.aleksey.appforevents.POJO.Feed;
import com.example.aleksey.appforevents.POJO.Radiation;
import com.example.aleksey.appforevents.POJO.Worker;
import com.example.aleksey.appforevents.POJO.Workers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.http.Url;

/**
 * Created by Aleksey on 25.01.2017.
 */


public class ListOfWorkers extends ListFragment implements SwipeRefreshLayout.OnRefreshListener {

    private OnWorkersInteractionListener WorkersListener;
    private SwipeRefreshLayout WorkersSwipeRefreshLayout;
    public static ArrayAdapter<String> WorkersListAdapter;
    FloatingActionButton fab;

    public interface OnWorkersInteractionListener {

        public void onWorkersInteraction(int position);
        public void onAddWorkerInteraction();
        public void UpdateListOfWorkers();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fab = (FloatingActionButton)getActivity().findViewById(R.id.fab_add_worker);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WorkersListener.onAddWorkerInteraction();
            }
        });
        WorkersListAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, Data.list_names_of_workers);
        setListAdapter(WorkersListAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.list_of_workers, null);
        WorkersSwipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipeWorkersList);
        WorkersSwipeRefreshLayout.setOnRefreshListener(this);
        return layout;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            WorkersListener = (OnWorkersInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " должен реализовывать интерфейс OnFragmentInteractionListener");
        }
    }

    public void onListItemClick (ListView l, View v, int pos, long id) {
        super.onListItemClick(l, v, pos, id); {
            updateDetail(pos);
        }
    }

    public void updateDetail(int position) {
        WorkersListener.onWorkersInteraction(position);
    }

    static String url = "http://192.168.59.43/";
    public static void get_List_Of_Workers(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API service = retrofit.create(API.class);
        Map<String,String> mapJsn = new HashMap<>();
        mapJsn.put("table","workers");
        Call<Workers> call = service.get_all_workers(mapJsn);

        call.enqueue(new Callback<Workers>() {
            @Override
            public void onResponse(Response<Workers> response, Retrofit retrofit) {
                Data.list_names_of_workers.clear();
                Data.list_of_workers.clear();
                for (Worker i : response.body().getWorkers()) {
                    Data.list_names_of_workers.add(i.getWorkerFname()+"\n"+i.getWorkerSname());
                    Data.list_of_workers.add(i);
                }
                WorkersListAdapter.notifyDataSetChanged();
                Log.d("Get Workers", "done");

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    public static void add_Worker(String worker_fname, String worker_sname, Integer worker_authdevice_id){
        Gson gson = new GsonBuilder().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API service = retrofit.create(API.class);
        Map<String,String> mapJsn = new HashMap<>();
        Worker worker = new Worker();
        worker.setWorkerFname(worker_fname);
        worker.setWorkerSname(worker_sname);
        worker.setWorkerAuthdeviceId(worker_authdevice_id);
        mapJsn.put("workers_data",gson.toJson(worker));
        Log.d("Worker",gson.toJson(worker));
        Call<Workers> call = service.add_worker(mapJsn);

        call.enqueue(new Callback<Workers>() {
            @Override
            public void onResponse(Response<Workers> response, Retrofit retrofit) {
                Data.list_names_of_workers.clear();
                Data.list_of_workers.clear();
                for (Worker i : response.body().getWorkers()) {
                    Data.list_names_of_workers.add(i.getWorkerFname()+"\n"+i.getWorkerSname());
                    Data.list_of_workers.add(i);
                }
                WorkersListAdapter.notifyDataSetChanged();
                Log.d("Get Workers", "done");
            }

            @Override
            public void onFailure(Throwable t) {
                Data.list_names_of_workers.clear();
                Data.list_of_workers.clear();
            }
        });
    }

    @Override
    public void onRefresh() {
        WorkersSwipeRefreshLayout.setRefreshing(true);
        WorkersSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                WorkersSwipeRefreshLayout.setRefreshing(false);
                //add_Worker("Николай","Трофимов",4);
                //get_List_Of_Workers();

            }
        }, 1000);

    }

}
