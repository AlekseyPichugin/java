package com.example.aleksey.appforevents;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aleksey.appforevents.POJO.Worker;
import com.example.aleksey.appforevents.POJO.Workers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class WorkersDetail extends android.support.v4.app.Fragment {


    private Integer REQUEST_CODE = 1;
    private Integer position;
    static String url = "http://192.168.59.43/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.workers_details, container, false);
        return view;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE){
            Integer answer = data.getIntExtra("Id",-1);
            if(answer != -1){

                change_authdevice(position,Data.list_of_workers.get(position).getWorkerId()
                        ,Data.list_of_authdevices.get(answer).getAuthdeviceId());

                Data.list_of_workers.get(position).setWorkerAuthdeviceId(Data.list_of_authdevices
                        .get(answer).getAuthdeviceId());

                Log.d("Request",String.valueOf(Data.list_of_authdevices
                        .get(answer).getAuthdeviceId()));
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        position = args.getInt("position");
        setText(position);
    }

    // обновление текстового поля
    public void setText(int position) {
        TextView f_name = (TextView)getActivity().findViewById(R.id.f_name);
        TextView s_name = (TextView)getActivity().findViewById(R.id.s_name);
        TextView authdevice = (TextView)getActivity().findViewById(R.id.authdevice);
        authdevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WorkersDetail.this.getActivity(), DetailsActivity.class);
                startActivityForResult(intent,REQUEST_CODE);
            }
        });

        f_name.setText(Data.list_of_workers.get(position).getWorkerFname());
        s_name.setText(Data.list_of_workers.get(position).getWorkerSname());
        if (Data.list_of_workers.get(position).getWorkerAuthdeviceId() != null) {
            authdevice.setText("ID устройства: " + String.valueOf(Data.list_of_workers
                    .get(position).getWorkerAuthdeviceId()));
        } else {
            authdevice.setText("ID устройства: No device");
        }
    }

    public static void change_authdevice(final Integer position, Integer worker_id, Integer authdevice_id){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API service = retrofit.create(API.class);
        Map<String,Integer> mapJsn = new HashMap<>();
        mapJsn.put("workers_id",worker_id);
        mapJsn.put("authdevice_id",authdevice_id);
        Call<Worker> call = service.change_authdevice(mapJsn);

        call.enqueue(new Callback<Worker>() {
            @Override
            public void onResponse(Response<Worker> response, Retrofit retrofit) {
                Data.list_of_workers.get(position).setWorkerAuthdeviceId(response.body()
                        .getWorkerAuthdeviceId());
                Log.d("change_authdevice","Good");
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }







}
